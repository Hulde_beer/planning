﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Planning.Model;

namespace Planning
{
    class Controller
    {

        public static void addDayToWeek(week w, day d) {
            d.week = w;

        }

        public static void assignShift(person p, shift s) {
            p.addShift(s);
            s.person = p;
        }
        public static void unassignShift(person p, shift s) {
            p.removeShift(s);
            s.person = null;
        }
    }
}
