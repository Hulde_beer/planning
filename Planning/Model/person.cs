﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Planning.Model
{
    public class person : INotifyPropertyChanged
    {
        private string _name;

        private TimeSpan _maxHours;


        public TimeSpan workingHours {
            get {
                TimeSpan hours = new TimeSpan();
                for (int i = 0; i < _shifts.Count; i++) {
                    hours.Add(_shifts[i].duration);
                }
                return hours;
            }
        }

        private List<shift> _shifts;

        public person() {
            _shifts = new List<shift>();

        }

        public void addShift(shift s) {
            this._shifts.Add(s);
            this.OnPropertyChanged(_PropertyNames.person.shifts);
        }
        public void removeShift(shift s) {
            _shifts.Remove(s);
            this.OnPropertyChanged(_PropertyNames.person.shifts);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
