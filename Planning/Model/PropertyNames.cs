﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planning.Model
{
    public class _PropertyNames
    {
        public class day {
            public const string dow = "dow";
            public const string shifts = "day_shifts";
            public const string week = "week";
        }
        public class week{

        }
        public class shift {
            public const string startTime = "startTime";
            public const string endTime = "endTime";
            public const string duration = "duration";
            public const string person = "person";
        }
        public class person {
            public const string shifts = "person_shifts";
        }
    }
}
