﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Planning.Model
{
    public class shift : INotifyPropertyChanged
    {
        #region private variables
        private TimeSpan _startTime;
        private TimeSpan _endTime;

        private person _person;
        #endregion


        public TimeSpan startTime {
            get { return _startTime; }
            set {
                if (!_startTime.Equals(value)) {
                    this.OnPropertyChanged(_PropertyNames.shift.startTime);
                    this.OnPropertyChanged(_PropertyNames.shift.duration);
                    this._startTime = value;
                }
            }
        }
        public TimeSpan endTime
        {
            get { return _endTime; }
            set
            {
                if (!_endTime.Equals(value))
                {
                    this.OnPropertyChanged(_PropertyNames.shift.endTime);
                    this.OnPropertyChanged(_PropertyNames.shift.duration);
                    this._endTime = value;
                }
            }
        }
        public TimeSpan duration {
            get {
                return _endTime.Subtract(_startTime);
            }
        }

        public person person{
            set {
                if (_person != value) {
                    this.OnPropertyChanged(_PropertyNames.shift.person);
                    _person = value;
                }
            }
            get { return _person; }
        }

        public bool isOpen {
            get {
                return this._person == null;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
