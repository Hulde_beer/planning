﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planning.Model
{
   

    public class day : INotifyPropertyChanged
    {
        #region private variables
        private DayOfWeek _dow;
        private week _week;

        private List<shift> _shifts;
        

        #endregion

        public day() {
            _shifts = new List<shift>();
        }

        public void addShift(shift s) {
            this._shifts.Add(s);
            this.OnPropertyChanged(_PropertyNames.day.shifts);
        }
        public void removeShift(shift s) {
            this._shifts.Remove(s);
            this.OnPropertyChanged(_PropertyNames.day.shifts);
        }

        #region properties
        public week week {
            get { return _week; }
            set {
                if (_week != value) {
                    this.OnPropertyChanged(_PropertyNames.day.week);
                }
                _week = value; }
        }

        public DayOfWeek DayOfWeek {
            set {
                if (value != _dow) {
                    this.OnPropertyChanged(_PropertyNames.day.dow);
                }
                _dow = value;
            }
        
            get {
                return _dow;
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
